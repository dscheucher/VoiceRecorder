Approach
Day 1: Build basic ui according to the design pictures
Day 2: program logic / functional things
    - saving / playing audio files
    - recording
    - permission handling
Day 3: Polish & UI upgrade
    - undo feature

Design decisions
    lombok for auto getter / setter generation of the recording model
    recyclerview to implement a swipeable list item
    support design lib for bottom sheet behaviour

What did not work, might be missing?
- Spent too much time to get AndroidX working and decided to stick with support libraries
- Record button hammering made the app crash