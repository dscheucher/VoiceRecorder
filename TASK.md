Setze das UI aus Design.sketch um. Du kannst die Resources aus der iOS Page entnehmen. Beim Design liefert die Android Page die Vorlage.

Der Microphone Button soll eine Audio-Aufnahme triggern und nach erneutem Tap die Aufnahme beenden.
Verwende für die Permission Abfrage die Android 6.0 (API 23) Permissions [1].
Dabei wird beim ersten Tap direkt die Systempermission angefordert, sollte diese beim ersten Mal verweigert werden,
verwende shouldShowRequestPermissionRationale und zeige einen Dialog bevor erneut die Systempermission angefordert wird.
Bei einem Tap auf "Recordings" soll sich eine Activity oder Fragment öffnen, der die Recordings anzeigt.
Ein Tap auf ein Recording erlaubt es dieses abzuspielen.
Die Recordings sollen auch wieder löschbar sein, wahlweise durch Swipe links/rechts oder Swipe und Bestätigen (siehe iOS Design).

- Generell einzuhalten:
    - Android Coding Standard [2], [3]
    - Best Practices [4]
    - Material Design Guidelines [5]
- Gradle und ProGuard verwenden (minifyEnabled: true)
- targetSdkVersion 24, minSdkVersion 16
- App für Right-To-Left Layouts optimieren
- Android 6.0 (API 23) Permissionabfrage
- Verwenden von Libraries: Begründung warum Library verwendet wurde
- Begründung der Entscheidung warum RecyclerView oder ListView bzw. Activities oder Fragments
- Beweise Kreativität mit Animationen

Bonus:
- Verwenden von ConstraintLayout (wo sinnvoll)
- "Undo delete Recording" mittels BottomSheet
- App basierend auf Model-View-ViewModel Pattern (MVVM)
- Unit Testing (Robolectric) / UI Testing (Robotium)
- Tabletoptimized


---
[1] https://developer.android.com/training/permissions/requesting.html
[2] https://source.android.com/source/code-style.html
[3] https://github.com/ribot/android-guidelines/blob/master/project_and_code_guidelines.md
[4] https://github.com/futurice/android-best-practices
[5] https://material.google.com/