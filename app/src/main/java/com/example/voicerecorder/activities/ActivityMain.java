package com.example.voicerecorder.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.voicerecorder.R;
import com.example.voicerecorder.utility.PermissionManager;
import com.example.voicerecorder.utility.Recorder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;

/**
 * The Main Activity holds the following components:
 * - record button: start / stops a record
 * - status text: holds information about the current status of the recording session
 * - Show Recordings button: opens an intend to the RecordingsActivity
 */
public class ActivityMain extends AppCompatActivity {
    private final String TIME_FORMAT = "yyyyMMdd_HHmmss";

    private Recorder mRecorder;
    private TextView mTxtStatus;
    private PulsatorLayout mPulsator;
    private PermissionManager mPermissionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecorder = new Recorder(this);
        mPermissionManager = new PermissionManager(this, getApplicationContext());

        mPulsator = (PulsatorLayout) findViewById(R.id.pulsator);
        mTxtStatus = (TextView) findViewById(R.id.text_status);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_show_recordings:
                startActivity(new Intent(this, ActivityRecordings.class));
                break;
            case R.id.button_microphone:
                if (!mPermissionManager.arePermissionsGranted())
                    mPermissionManager.requestPermissions();
                else if (!mRecorder.isRecording()) {
                    mRecorder.start(getTimeAsString());
                    mPulsator.start();
                } else {
                    mRecorder.stop();
                    mPulsator.stop();
                }

                // update status text
                if (mRecorder.isRecording())
                    mTxtStatus.setText(R.string.text_status_recording);
                else mTxtStatus.setText(getString(R.string.text_status_idle));
                break;
        }
    }

    public String getTimeAsString() {
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
        return sdf.format(new Date());
    }
}
