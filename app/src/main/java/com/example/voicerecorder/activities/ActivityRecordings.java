package com.example.voicerecorder.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.voicerecorder.R;
import com.example.voicerecorder.models.Recording;
import com.example.voicerecorder.ui.DividerItemDecoration;
import com.example.voicerecorder.ui.RecyclerItemTouchHelper;
import com.example.voicerecorder.ui.RecyclerViewAdapterRecording;
import com.example.voicerecorder.utility.Recorder;

import java.util.ArrayList;
import java.util.List;

/**
 * The Recordings Activity holds all recordings and displays them in a list
 * <p>
 * What is does besides that:
 * - each item is display via a custom adapter / view holder that show the name of the file, which
 * is currently the date and time and the audio duration.
 * - each item has its own ClickListener which loads the file to a MediaPlayer
 * to play it.
 * - when an item triggers a swipe event (which means the user wishes to deletes it from the list)
 * a thread gets initialized that deletes the item after a given delay, expect the user his the
 * undo bottom sheet in time and therefore sets a "skip delete"-flag for the connected thread.
 */
public class ActivityRecordings extends AppCompatActivity {
    private final int DELAY_MS_UNDO = 3000;

    // flag defines if a user pressed the bottom sheet and therefore wishes to undo
    // therefore only the last deleted item gets recovered
    private List<Boolean> mUndoConfirmed;

    // holds all for deletion scheduled recordings
    private List<Recording> mDeleteScheduledRecordings;

    // holds the number of bottom sheet threads
    // bottom sheet is only hidden when the last thread is about to end
    private int mActiveThreads = 0;

    private BottomSheetBehavior mBottomSheetBehavior;
    private Button mBtnUndo;
    private TextView mTxtEmpty;

    // list view
    private RecyclerView mListRecordings;
    private RecyclerViewAdapterRecording mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordings);

        mUndoConfirmed = new ArrayList<>();
        mDeleteScheduledRecordings = new ArrayList<>();

        mBtnUndo = (Button) this.findViewById(R.id.bottom_sheet_button);
        mListRecordings = (RecyclerView) findViewById(R.id.list_recordings);
        mTxtEmpty = (TextView) findViewById(R.id.text_empty);

        // hide bottom sheet
        LinearLayout llBottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        // set item divider
        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        mListRecordings.addItemDecoration(dividerItemDecoration);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mListRecordings.setLayoutManager(layoutManager);
        mListRecordings.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new RecyclerViewAdapterRecording(this, Recorder.getAllRecordings());
        mListRecordings.setAdapter(mAdapter);

        // trigger event whenever an item is swiped
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback =
                new RecyclerItemTouchHelper(0,
                        ItemTouchHelper.LEFT,
                        new RecyclerItemTouchHelper.RecyclerItemTouchHelperListener() {
                            @Override
                            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction, int position) {
                                if (viewHolder instanceof RecyclerViewAdapterRecording.RecordingViewHolder) {

                                    final Recording recording =
                                            ((RecyclerViewAdapterRecording.RecordingViewHolder) viewHolder).recording;
                                    // save item index
                                    final int deletedIndex = viewHolder.getAdapterPosition();

                                    mAdapter.removeItem(viewHolder.getAdapterPosition());
                                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                                    // check if no items are currently in the list and show feedback
                                    updateEmptyListFeedback();
                                    // Start new thread to close undo sheet
                                    final Thread undoTimer = createUndoThread(recording);
                                    mUndoConfirmed.add(false);
                                    mDeleteScheduledRecordings.add(recording);
                                    undoTimer.start();
                                    mActiveThreads++;

                                    // recover item when bottom sheet undo option is clicked
                                    mBtnUndo.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                                            mUndoConfirmed.set(mActiveThreads - 1, true);

                                            // in case of unlucky timing with thread
                                            // ensure that the file still exists
                                            if (recording.exists())
                                                mAdapter.restoreItem(recording, deletedIndex);
                                            updateEmptyListFeedback();
                                        }
                                    });
                                }
                            }
                        });

        // attach callback to recyclerview
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mListRecordings);

        updateEmptyListFeedback();
    }

    // Delete missing scheduled recordings
    // if that is not done recordings might reappear in the list view when the activity is opened
    // again but the delay thread has not reached the runOnUiThread part yet
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        for (Recording recording : mDeleteScheduledRecordings)
            Recorder.deleteRecording(recording);
    }

    /**
     * check if list is empty and sets visibility flags for the list and the feedback text
     */
    private void updateEmptyListFeedback() {
        if (mAdapter.getItemCount() == 0) {
            mListRecordings.setVisibility(View.GONE);
            mTxtEmpty.setVisibility(View.VISIBLE);
        } else {
            mTxtEmpty.setVisibility(View.GONE);
            mListRecordings.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Creates a new Thread that deletes a recording if the undo flag for that thread is not set
     */
    private Thread createUndoThread(final Recording recording) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(DELAY_MS_UNDO);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // get undo flag for current thread
                if (!mUndoConfirmed.get(mActiveThreads - 1)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Delete file FOREVER
                            if (recording.exists())
                                Recorder.deleteRecording(recording);

                            // if this is the last thread waiting for undo, hide the undo sheet
                            if (mActiveThreads == 1)
                                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                            mActiveThreads--;
                            mUndoConfirmed.remove(mActiveThreads);
                            mDeleteScheduledRecordings.remove(recording);
                        }
                    });
                } else {
                    mActiveThreads--;
                    mUndoConfirmed.remove(mActiveThreads);
                    mDeleteScheduledRecordings.remove(recording);
                }
            }
        };
        return thread;
    }
}
