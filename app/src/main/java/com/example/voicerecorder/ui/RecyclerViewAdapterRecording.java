package com.example.voicerecorder.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.voicerecorder.R;
import com.example.voicerecorder.models.Recording;
import com.example.voicerecorder.utility.MediaPlayerRecording;

import java.util.List;

/**
 * Connects the recording items in a recording recycle view to the recording data objects.
 */
public class RecyclerViewAdapterRecording extends RecyclerView.Adapter<RecyclerViewAdapterRecording.RecordingViewHolder> {
    private final Context context;
    private List<Recording> recordings;
    private MediaPlayerRecording player;

    private int selectedPos = RecyclerView.NO_POSITION;

    public RecyclerViewAdapterRecording(Context context, List<Recording> recordings) {
        this.context = context;
        this.recordings = recordings;
        player = new MediaPlayerRecording();
    }

    // remove an item and update view
    public void removeItem(int position) {
        recordings.remove(position);
        notifyItemRemoved(position);
    }

    // reinsert an item and update view
    public void restoreItem(Recording item, int position) {
        recordings.add(position, item);
        notifyItemInserted(position);
    }

    @Override
    public RecordingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_recording, parent, false);
        final RecordingViewHolder viewHolder = new RecordingViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecordingViewHolder holder, final int position) {
        final Recording item = recordings.get(position);
        holder.recording = item;

        holder.name.setText(item.getDescription());
        holder.duration.setText(item.getDuration(context));

        // attach on click listener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // if the player is already playing check if same recording was selected again
                // in that case stop the recording
                // otherwise just start the new recording
                if (player.isPlaying()) {
                    player.stop();
                    if (player.getLastRecording() != holder.recording)
                        player.start(holder.recording);
                } else player.start(holder.recording);

                // unselect currently selected item
                notifyItemChanged(selectedPos);
                selectedPos = position;
                // select new item
                notifyItemChanged(selectedPos);
            }
        });

        // handle selected state of item
        holder.itemView.setSelected(selectedPos == position);
    }

    @Override
    public int getItemCount() {
        return recordings.size();
    }

    // Compose ViewHolder class
    public class RecordingViewHolder extends RecyclerView.ViewHolder {
        public TextView name, duration;
        public RelativeLayout viewBackground, viewForeground;
        public Recording recording;

        public RecordingViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.textName);
            duration = (TextView) view.findViewById(R.id.textDuration);
            viewBackground = (RelativeLayout) view.findViewById(R.id.view_background);
            viewForeground = (RelativeLayout) view.findViewById(R.id.view_foreground);
        }
    }
}