package com.example.voicerecorder.utility;

import android.media.MediaPlayer;

import com.example.voicerecorder.models.Recording;

import java.io.IOException;

/**
 * The MediaPlayerRecording class holds a MediaPlayer
 * and allows to start / stop playing a recording.
 */
public class MediaPlayerRecording {
    private MediaPlayer mPlayer;
    private Recording mLastRecording;

    public void start(Recording recording) {
        this.mPlayer = new MediaPlayer();
        this.mLastRecording = recording;
        try {
            mPlayer.setDataSource(recording.getPath());
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        mPlayer.release();
        mPlayer = null;
    }

    public boolean isPlaying() {
        if (mPlayer == null) return false;
        return mPlayer.isPlaying();
    }

    public Recording getLastRecording() {
        return this.mLastRecording;
    }
}
