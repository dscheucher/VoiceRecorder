package com.example.voicerecorder.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.example.voicerecorder.R;

/**
 * The PermissionManager handles all the required permissions the app needs
 */
public class PermissionManager {
    private final String PERM_RECORD = Manifest.permission.RECORD_AUDIO;
    private final String PERM_STORAGE_WRITE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String PERM_STORAGE_READ = Manifest.permission.READ_EXTERNAL_STORAGE;

    private Context mContext;
    private Activity mActivity;
    private String[] mPermissions;

    public PermissionManager(Activity mActivity, Context context) {
        this.mContext = context;
        this.mActivity = mActivity;

        this.mPermissions = new String[]{
                PERM_RECORD,
                PERM_STORAGE_WRITE,
                PERM_STORAGE_READ
        };
    }

    /**
     * Requests all permissions that are left
     * In case the user already denied the request once, a simple dialog will be shown
     * to emphasise the need to grant these permissions
     */
    public void requestPermissions() {
        boolean showDialog = false;
        for (String permission : mPermissions) {
            // check if permission is already granted
            if (ContextCompat.checkSelfPermission(mContext, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {
                    showDialog = true;
                }
            }
        }

        if (showDialog) showDialog();
        else ActivityCompat.requestPermissions(mActivity, mPermissions, 200);
    }

    public boolean arePermissionsGranted() {
        boolean granted = true;
        for (String permission : mPermissions) {
            // check if permission is already granted
            if (ContextCompat.checkSelfPermission(mContext, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                granted = false;
            }
        }
        return granted;
    }

    private void showDialog() {
        Resources resources = mContext.getResources();
        AlertDialog alertDialog = new AlertDialog.Builder(mActivity).create();
        alertDialog.setMessage(resources.getString(R.string.dialog_text));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, resources.getString(R.string.allow),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(mActivity, mPermissions, 200);
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, resources.getString(R.string.deny),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
