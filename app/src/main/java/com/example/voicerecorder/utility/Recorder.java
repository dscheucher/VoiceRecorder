package com.example.voicerecorder.utility;

import android.content.Context;
import android.media.MediaRecorder;
import android.os.Environment;

import com.example.voicerecorder.R;
import com.example.voicerecorder.models.Recording;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Holds control methods to record a new audio file, delete an existing one and
 * methods to acquire information about what files go already recorded.
 */
public class Recorder {
    private static String sDirPath;
    private static String sCurrentFile = null;
    private final String SEP_OPERATOR = File.separator;
    private MediaRecorder mRecorder;
    private boolean mIsRecording;

    public Recorder(Context context) {
        sDirPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                + SEP_OPERATOR
                + context.getString(R.string.app_name);
        new File(sDirPath).mkdir();
        this.mIsRecording = false;
    }

    /**
     * @return a list of all recordings present to date,
     * excluding the file that is currently being recorded
     */
    public static ArrayList<Recording> getAllRecordings() {
        ArrayList<Recording> recordings = new ArrayList<>();
        File[] files = new File(sDirPath).listFiles();

        // check if directory is not empty to avoid a NullPointerException
        if (files != null) {
            for (File file : files) {

                // opt out currently recording file as it is not stable
                String filePath = file.getName();
                if (!filePath.equals(sCurrentFile)) {
                    recordings.add(
                            new Recording(
                                    file.getName(),
                                    file.getAbsolutePath()
                            ));
                }
            }
        }
        return recordings;
    }

    public static boolean deleteRecording(Recording recording) {
        return new File(recording.getPath()).delete();
    }

    public void start(String name) {
        if (!mIsRecording) {
            sCurrentFile = name;

            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

            mRecorder.setOutputFile(sDirPath + SEP_OPERATOR + name);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            try {
                mRecorder.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mRecorder.start();
            mIsRecording = true;
        }
    }

    public void stop() {
        try {
            mRecorder.stop();
        } catch (RuntimeException e) {
            e.printStackTrace();
            // stop failed: If stop is immediately called after start the recorder crashes
            // maybe be due to a basically non existent record
            // TODO: find a way to avoid an error for start / stop hammering
        }
        mRecorder.release();
        mRecorder = null;
        mIsRecording = false;
        sCurrentFile = null;
    }

    public boolean isRecording() {
        return mIsRecording;
    }
}
